@IsTest(SeeAllData=True)
public class AccountCreateEntitlement_Test 
{
    public static testMethod void testAccountCreateEntitlement() 
    {
        // Test code for Entitlement trigger
        
        // Create various flavours of account
        Account ac = new Account(Name='Fred Bloggs 22227 Plc',BillingCountry='United Kingdom',Entitlement_Level__c='Silver',
                                 Customer_Type__c='B2B',Account_Type_Id__c='1');
        insert ac;

        Account ac1 = new Account(Name='Fred Bloggs 22228 Plc',BillingCountry='United Kingdom',Entitlement_Level__c='Bronze',
                                 Customer_Type__c='B2C',Account_Type_Id__c='2');
        insert ac1;

        Account ac2 = new Account(Name='Fred Bloggs 22229 Plc',BillingCountry='United Kingdom',Entitlement_Level__c='Bronze',
                                 Customer_Type__c='B2C',Account_Type_Id__c='3',CurrencyISOCode='GBP');
        insert ac2;
        
        // Create a contact
        Contact co = new Contact(FirstName='Fred',LastName='Bloggs',AccountId=ac2.Id,Subscriber_Type__c='ZZZ');
        insert co;

        // Create a contact without a subscriber type
        Contact co1 = new Contact(FirstName='Fred',LastName='Bloggs2',AccountId=ac2.Id,Subscriber_Type__c=null);
        insert co1;
        
        // Change type and country for one
        ac2.BillingCountry='United States';
        ac2.Customer_Type__c='B2B';
        ac2.Account_Type_Id__c='1';
        ac2.Entitlement_Level__c='Silver';
        ac2.CurrencyISOCode='USD';
        update ac2;

        ac2.BillingCountry=null;
        update ac2;
        
        ac2.Customer_Type__c='B2C';
        ac2.Account_Type_Id__c='1';
        update ac2;

        ac2.Account_Type_Id__c='2';
        update ac2;

        ac2.Account_Type_Id__c='1';
        update ac2;
    }
}